# G4 Nucleo Sandbox

This is a main application, the third of three components comprising the firmware update architecture outlined in
[Memfault's blog article](https://interrupt.memfault.com/blog/device-firmware-update-cookbook#dfu-failures-should-not-brick-the-device).

The components are, or will be eventually:

0. the [Bootloader][]
1. the [App Loader][]
2. the main application (this project)
3. the Loader Updater

[Bootloader]: https://gitlab.com/starcopter/embedded/bootloader
[App Loader]: https://gitlab.com/starcopter/embedded/app-loader

[[_TOC_]]

## Hacking

This repository contains configuration for the [VSCode][vscode] IDE. Following the steps below you will be able to
connect to a remote development server and work on the Nucleo hardware attached to it.

### ZeroTier Networking

The tunnel into the machine assigned to you runs on a [ZeroTier][zerotier] VPN, which requires installation of the
[ZeroTier client][zerotier-download] on your machine.

> _Note_: Only step 2 "Download ZeroTier" is required to join the relevant network, neither the creation of a ZeroTier
> account (step 1) nor managing a network via the admin console (step 3) are needed in this case.

With the ZeroTier client installed, request to join the network `db64858fedc66322` and notify Lasse of your own
ten-digit ZeroTier address. Once Lasse has accepted your request, you will be able to reach the remote server assigned
to you via SSH.

### Local VSCode Installation and Setup

First, you will need to [install Visual Studio Code][vscode-download] on your local machine. Once VSCode itself is
installed, you will also need to install the VSCode [Remote - SSH][remote-ssh] extension to connect your local VSCode
instance to the remote development server.

For VSCode to be able to connect to the development server, add a host block to your [SSH Config File][ssh-config]
at `$HOME/.ssh/config` (or equivalent). Create the file if necessary. _Important: it's `config`, not `config.txt`!_

```
Host stm32-devel
    # replace the IP address with the one given to you
    HostName 192.168.192.<YOUR MACHINE>
    User <YOUR USERNAME>
```

Connect to the remote development machine via the remote explorer in the left sidebar.
On the remote machine, open the preconfigured repository at `~/src/g4-nucleo/`.

![VSCode](img/vscode.png)

### Compiling, Debugging and Device Interaction

There are three (primary) ways to interact with the connected prototyping hardware: directly interfacing through
[pyocd][] commands, using the [pyric][] device interface, or on-chip debugging from inside VSCode.

#### Serial Console

The G4 Nucleo has a serial console connected to the host system, usually available at `/dev/ttyACM0`.
Currently, this console is only used for logging output. The streaming log output can be accessed with `make console`.

#### pyocd

The G4 microcontroller is connected to the host system via an ST-Link V3, and thus can be managed via [pyocd][].

| Command              | Description                                                            |
| -------------------- | ---------------------------------------------------------------------- |
| `make reset`         | reset/restart/reboot the device, executes `pyocd reset` under the hood |
| `make halt`          | halt/pause the device (runs `pyocd commander -c halt`)                 |
| `make continue`      | resume device after halting (runs `pyocd commander -c continue`)       |
| `make run-gdbserver` | start a GDB server for a [remote debug session][gdb-remote]            |

For more information on available pyocd commands, consult `pyocd --help` and the [online documentation][pyocd-docs].

#### pyric

[`pyric`][pyric] is (among other things) an in-house CLI tool to interact with application images and devices.
This tool is used by `make flash` to flash the compiled application onto the target.
Refer to `pyric --help` and `pyric device --help` for further documentation.

#### VSCode On-Chip Debugging

Via the [Cortex-Debug][cortex-debug] extension, VSCode is capable of On-Chip debugging remote targets.
A new debug session can be initiated by pressing <kbd>F5</kbd>.

> _Note_: In some cases the device freezes up after disconnecting from an interactive debug session.
> Execution can be resumed by the above mentioned commands `make continue` or `make reset`.

[zerotier]: https://www.zerotier.com/
[zerotier-download]: https://www.zerotier.com/download/
[vscode]: https://code.visualstudio.com/
[vscode-download]: https://code.visualstudio.com/download
[remote-ssh]: https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-ssh
[ssh-config]: https://linuxize.com/post/using-the-ssh-config-file/
[pyocd]: https://pyocd.io/
[pyric]: https://gitlab.com/starcopter/embedded/py-aeric
[gdb-remote]: https://pyocd.io/docs/gdb_setup.html
[pyocd-docs]: https://pyocd.io/docs/target_support.html
[cortex-debug]: https://marketplace.visualstudio.com/items?itemName=marus25.cortex-debug

## Memory Layout

This application is not natively executable, it has to be launched by the [Bootloader][].

```
0x08020000 ┌─────────────────────┐
           │     2K NVM Data     │ ◄─────  EEPROM-like storage for persistent application data
0x0801f800 ├─────────────────────┤
           │                     │
           .         84K         .         arbitrary number of page-aligned applications
           .  Application Space  . ◄─────  every application has to start with an image header
           .  Page (2K) Aligned  .         the app loader discovers bootable apps and launches them
           │                     │
0x08008000 ├─────────────────────┤
           │                     │         application loader and updater, launched by bootloader
           │    40K App Loader   │ ◄─────  either launches or updates apps in application space
           │                     │         communicates with image file server via UAVCAN
0x08000800 ├─────────────────────┤
           │    512 Bytes DCB    │ ◄─────  read-only device configuration block
0x08000600 ├─────────────────────┤
           │   1.5K Bootloader   │
0x08000000 └─────────────────────┘
```

For further details about the boot and software update process, see the [Bootloader][] and [App Loader][] projects.

## Processor Configuration

![Pinout](img/nucleo-g4-pinout.png)

### Pin Configuration

The following table depicts the Nucleo's pin configuration.
Duplicate rows show alternate configuration options, with the (currently) selected configuration highlighted in bold.

| Pin      | Signal     | Mode   | OType | OSpeed    | Pull |   AF | Peripheral   | Function            |
| -------- | ---------- | ------ | ----- | --------- | ---- | ---: | ------------ | ------------------- |
| **PA0**  | `PWM_IN`   | Input  |       |           | Down |      |              | PWM Input           |
| PA0      | `PWM_IN`   | Input  |       |           | Down |      | `EXTI0`      | PWM Input           |
| PA0      | `PWM_IN`   | Input  |       |           | Down |    1 | `TIM2_CH1`   | PWM Input           |
| PA0      | `PWM_IN`   | Input  |       |           | Down |      | ???          | PWM Input           |
| **PA2**  | `DEBUG_TX` | AF     | PP    | Medium    |      |    7 | `USART2_TX`  | Console             |
| **PA3**  | `DEBUG_RX` | AF     |       |           |      |    7 | `USART2_RX`  | Console             |
| **PA11** | `CAN_RX`   | AF     |       |           |      |    9 | `FDCAN1_RX`  | CAN                 |
| **PA12** | `CAN_TX`   | AF     | PP    | Medium    |      |    9 | `FDCAN1_TX`  | CAN                 |
| **PA13** | `SWDIO`    | AF     | PP    | Very High | Up   |    0 | `SWDIO_JTMS` |                     |
| **PA14** | `SWCLK`    | AF     |       |           | Down |    0 | `SWCLK_JTCK` |                     |
| **PA15** | `JTDI`     | AF     |       |           | Up   |    0 | `JTDI`       |                     |
| **PB3**  | `SWO`      | AF     | PP    | Medium    |      |    0 | `SWO_JTDO`   |                     |
| **PB8**  | `LED`      | Output | PP    | Low       |      |      |              | LED Output (direct) |
| PB8      | `LED`      | AF     | PP    | Low       |      |    6 | `TIM4_CH3`   | LED Output (PWM)    |
| **PG10** | `RST`      |        |       |           |      |      |              | Reset Button        |

### Subsystems

The section below summarizes the utilization of G4 hardware peripherals in this project.

#### USART2

Debug console (logging).

#### TIM3

Timekeeper clock and FDCAN hardware timestamping.

#### FDCAN1

CAN (Cyphal) interface, handled by the [protocol stack shared-utilities/](submodules/shared-utilities/canard/).

#### CRC

Hardware CRC unit.

#### RNG

Random number generator, currently only used to generate a random boot ID exposed via the **com.starcopter.boot_id**
Cyphal register.

### Interrupts

The STM32G4 uses 16 interrupt priorities, where a lower numerical value means higher priority.
Interrupts running at priority 4 and lower are safe to call FreeRTOS API functions,
interrupts running at priority 3 and higher are above FreeRTOS.

| Interrupt    | Priority | Purpose                                         |
| ------------ | -------: | ----------------------------------------------- |
| `SVC`        |        0 | starting FreeRTOS                               |
| `FDCAN1_IT1` |        6 | notifying the Transport task of bus events      |
| `TIM3`       |        8 | Timekeeper                                      |
| `USART2`     |       14 | TX FIFO empty, copy characters from ring buffer |
| `SysTick`    |       15 | FreeRTOS scheduler tick                         |
| `PendSV`     |       15 | something FreeRTOS                              |

### FreeRTOS Tasks

FreeRTOS is [configured](src/FreeRTOSConfig.h) with 8 different task priorities, mirroring the Cyphal transfer
priorities. Not all of these priorities are currently in use, however. A higher numerical value means higher priority.

| Task           | Priority | File               | Purpose                                                          |
| -------------- | -------: | ------------------ | ---------------------------------------------------------------- |
| Transport      |        6 | [transport.c][]    | Cyphal (via CAN FD) transport worker task, handles communication |
| Timer Svc      |        5 | [timers.c][]       | Service task for [FreeRTOS Timers][]                             |
| PWM Input      |        3 | [pwm-input.c][]    | PWM input sandbox task                                           |
| Node Monitor   |        2 | [node-monitor.c][] | Network monitor, tracks other nodes on the network               |
| GetInfo xxx/yy |        2 | [node.c][]         | Spawned to handle uavcan.node.GetInfo requests                   |
| xCmd xxx/yy    |        2 | [node.c][]         | Spawned to handle uavcan.node.ExecuteCommand requests            |
| List xxx/yy    |        1 | [registry.c][]     | Spawned to handle uavcan.register.List requests                  |
| Access xxx/yy  |        1 | [registry.c][]     | Spawned to handle uavcan.register.Access requests                |
| Main           |        1 | [main.c][]         | Main task, started first and responsible to start other tasks    |
| IDLE           |        0 | [tasks.c][]        | FreeRTOS background task, see [docs][idle task] for details      |

[main.c]: src/apps/main.c
[node-monitor.c]: src/apps/node-monitor.c
[pwm-input.c]: src/apps/pwm-input.c
[tasks.c]: submodules/freertos/kernel/tasks.c
[transport.c]: submodules/shared-utilities/canard/transport.c
[node.c]: submodules/shared-utilities/canard/node.c
[registry.c]: submodules/shared-utilities/sclib/registry.c
[timers.c]: submodules/freertos/kernel/timers.c
[FreeRTOS Timers]: https://www.freertos.org/RTOS-software-timer.html
[idle task]: https://www.freertos.org/RTOS-idle-task.html

## Bootstrap New Processor

To get a new G4 Nucleo up and running, a few bootstrapping steps have to be completed first.

### Boot Mode via Option Bytes

The desired option byte configuration is as follows:

| Option Byte | Register     | Value |
| ----------- | ------------ | :---: |
| `nBOOT0`    | `FLASH_OPTR` |   1   |
| `nSWBOOT0`  | `FLASH_OPTR` |   0   |

As far as I know, this is (should be? could be?) the default configuration.

The option bytes can be programmed in various ways, one of these being the [STM32CubeProgrammer][STM32CubeProg]
provided by STMicroelectronics.

  [STM32CubeProg]: https://www.st.com/content/st_com/en/products/development-tools/software-development-tools/stm32-software-development-tools/stm32-programmers/stm32cubeprog.html

### Bootloader and DCB

The platform shall be provisioned under the name `com.starcopter.nucleo.g4` with hardware release `A.1`.

```shell
> pyric device provision \
    --no-confirm \
    --bootloader ../bootloader \
    --app ../app-loader \
    --app . \
    com.starcopter.nucleo.g4 A.1

--------  ---------------------------------
DCB type  DeviceConfigurationV1
device    com.starcopter.nucleo.g4
release   A.1
date      2022-09-12 12:41:02 CEST
uid       b650aa86583c3562631f0cbe9365aced
chip      lot='98M16P', wafer=7, x=41, y=91
--------  ---------------------------------

Address Range           Type         Name                                 Version    GNU Build ID                              Git SHA    Timestamp
----------------------  -----------  -----------------------------------  ---------  ----------------------------------------  ---------  ------------------------
0x08000000..0x08000600  NativeImage  bootloader.bin                       v0.0.0     56371b082f9145b1ef681a0987a099cc0e71bc41             2022-05-02 19:35:21 CEST
0x08000800..0x0800a0f0  AppImageV3   com.starcopter.nucleo.g4.app-loader  v0.1.5     c2bd21d05b7089c8e2b006175f08d50863a8c642  ad12c1f0   2022-09-09 16:29:00 CEST
0x0800a800..0x08014f78  AppImageV3   com.starcopter.nucleo.g4             v0.1.2-17  0238eb4c3240157bec78ac254b54c3e94608f507  afc47f24   2022-09-15 01:01:04 CEST

[==================================================] 100%
```

  [RM0440]: doc/RM0440%20-%20STM32G4%20Series%20Reference%20Manual.pdf
  [DS12589]: doc/DS12589%20-%20STM32G431xB%20Datasheet.pdf
