/**
 * @file main.c
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief Application Loader
 * @date 2021-06-14
 *
 * @copyright Copyright (c) 2021 starcopter GmbH
 *
 * This software is distributed under the terms of the MIT License.
 *
 */

#include "main.h"

#include <FreeRTOS.h>
#include <task.h>

#include <sclib/crc.h>
#include <sclib/console.h>
#include <sclib/timekeeper.h>
#include <sclib/registry.h>
#include <canard/fdcan.h>

#include "apps/apps.h"

#define LOG_LEVEL LOG_LEVEL_TRACE
#include <sclib/log.h>

extern void* __VECTOR_TABLE;

ImageHeader_t image_hdr __attribute__((section(".image_hdr"))) = {
    .image_magic  = IMAGE_MAGIC,
    .header_start = (uint32_t*) &image_hdr,
    .vector_table = (uint32_t*) &__VECTOR_TABLE,
    .header_size  = sizeof(ImageHeader_t),
    .image_end    = (uint32_t*) &__image_end,

    .image_hdr_version = eHEADER_VERSION_CURRENT,
    .clean_build       = GIT_CLEAN ? 1 : 0,  // set in Makefile
    .version =
        {
            .major   = VERSION_MAJOR,   // set in Makefile
            .minor   = VERSION_MINOR,   // set in Makefile
            .patch   = VERSION_PATCH,   // set in Makefile
            .commits = VERSION_COMMITS  // set in Makefile
        },
    .min_upgrade_from   = {.major = 0, .minor = 0, .patch = 0, .commits = 0},
    .max_downgrade_from = {.major = VERSION_MAJOR, .minor = VERSION_MINOR, .patch = 255, .commits = 255},
    .target_hardware    = {.major = 'A', .minor = 2},
    .min_hardware       = {.major = 'A', .minor = 0},
    .time_utc           = BUILD_DATE,      // set in Makefile
    .git_sha            = GIT_COMMIT_ARR,  // set in Makefile
    .name               = "com.starcopter.nucleo.g4",

    .header_crc = 0x12344321,  // populated after linking
};

void init_fdcan(void);
void reinit_fdcan(void);
void generate_boot_id(void);

static void init(void);

int main(void) {
    init_timekeeper();
    init();
    generate_boot_id();

    start_main_task();

    vTaskStartScheduler();

    WTF();
    return 1;
}

static void init(void) {
    RCC->APB1ENR1 |= RCC_APB1ENR1_TIM2EN      // PWM input
                     | RCC_APB1ENR1_TIM3EN    // Timekeeper
                     | RCC_APB1ENR1_TIM4EN    // LED dimming
                     | RCC_APB1ENR1_USART2EN  // Debug Console
                     | RCC_APB1ENR1_FDCANEN;  // CAN
    RCC->APB2ENR |= RCC_APB2ENR_SYSCFGEN;     // System Configuration Controller
    RCC->AHB1ENR |= RCC_AHB1ENR_CRCEN;        // CRC
    RCC->AHB2ENR |= RCC_AHB2ENR_RNGEN         // Boot ID and other randomness
                    | RCC_AHB2ENR_GPIOAEN     // Port A
                    | RCC_AHB2ENR_GPIOBEN;    // Port B

    SET_BIT(RNG->CR, RNG_CR_RNGEN);  // start random number generator; it requires some time to warm up

    /*
     * Pin Configuration
     * -----------------
     *
     * | Pin      | Signal     | Mode   | OType | OSpeed    | Pull |   AF | Peripheral   | Function            |
     * | -------- | ---------- | ------ | ----- | --------- | ---- | ---: | ------------ | ------------------- |
     * | **PA0**  | `PWM_IN`   | Input  |       |           | Down |      |              | PWM Input           |
     * | PA0      | `PWM_IN`   | Input  |       |           | Down |      | `EXTI0`      | PWM Input           |
     * | PA0      | `PWM_IN`   | Input  |       |           | Down |    1 | `TIM2_CH1`   | PWM Input           |
     * | PA0      | `PWM_IN`   | Input  |       |           | Down |      | ???          | PWM Input           |
     * | **PA2**  | `DEBUG_TX` | AF     | PP    | Medium    |      |    7 | `USART2_TX`  | Console             |
     * | **PA3**  | `DEBUG_RX` | AF     |       |           |      |    7 | `USART2_RX`  | Console             |
     * | **PA11** | `CAN_RX`   | AF     |       |           |      |    9 | `FDCAN1_RX`  | CAN                 |
     * | **PA12** | `CAN_TX`   | AF     | PP    | Medium    |      |    9 | `FDCAN1_TX`  | CAN                 |
     * | **PA13** | `SWDIO`    | AF     | PP    | Very High | Up   |    0 | `SWDIO_JTMS` |                     |
     * | **PA14** | `SWCLK`    | AF     |       |           | Down |    0 | `SWCLK_JTCK` |                     |
     * | **PA15** | `JTDI`     | AF     |       |           | Up   |    0 | `JTDI`       |                     |
     * | **PB3**  | `SWO`      | AF     | PP    | Medium    |      |    0 | `SWO_JTDO`   |                     |
     * | **PB8**  | `LED`      | Output | PP    | Low       |      |      |              | LED Output (direct) |
     * | PB8      | `LED`      | AF     | PP    | Low       |      |    6 | `TIM4_CH3`   | LED Output (PWM)    |
     * | **PG10** | `RST`      |        |       |           |      |      |              | Reset Button        |
     *
     * */

    GPIOA->AFR[0] = (7ul << GPIO_AFRL_AFSEL2_Pos)           // DEBUG_RX       AF7: USART2_TX
                    | (7ul << GPIO_AFRL_AFSEL3_Pos);        // DEBUG_TX       AF7: USART2_RX
    GPIOA->AFR[1] = (9ul << GPIO_AFRH_AFSEL11_Pos)          // CAN3_RX        AF9: FDCAN1_RX
                    | (9ul << GPIO_AFRH_AFSEL12_Pos)        // CAN3_TX        AF9: FDCAN1_TX
                    | (0ul << GPIO_AFRH_AFSEL13_Pos)        // SWDIO          AF0: SWDIO_JTMS
                    | (0ul << GPIO_AFRH_AFSEL14_Pos)        // SWCLK          AF0: SWCLK_JTCK
                    | (0ul << GPIO_AFRH_AFSEL15_Pos);       // JTDI           AF0: JTDI
    GPIOB->AFR[0] = (0ul << GPIO_AFRL_AFSEL3_Pos);          // SWO            AF0: SWO_JTDO
    GPIOA->PUPDR  = (2ul << GPIO_PUPDR_PUPD0_Pos)           // PWM_IN         Pull-Down
                   | (1ul << GPIO_PUPDR_PUPD13_Pos)         // SWDIO_JTMS     Pull-Up
                   | (2ul << GPIO_PUPDR_PUPD14_Pos)         // SWCLK_JTCK     Pull-Down
                   | (1ul << GPIO_PUPDR_PUPD15_Pos);        // JTDI           Pull-Up
    GPIOA->OSPEEDR = (1ul << GPIO_OSPEEDR_OSPEED2_Pos)      // DEBUG_TX       Medium Speed
                     | (1ul << GPIO_OSPEEDR_OSPEED12_Pos)   // CAN_TX         Medium Speed
                     | (3ul << GPIO_OSPEEDR_OSPEED13_Pos);  // SWDIO          Very-High Speed
    GPIOB->OSPEEDR = (1ul << GPIO_OSPEEDR_OSPEED3_Pos);     // SWO            Medium Speed
    GPIOA->MODER   = (0ul << GPIO_MODER_MODE0_Pos)          // PWM_IN         Input
                   | (2ul << GPIO_MODER_MODE2_Pos)          // DEBUG_TX       AF
                   | (2ul << GPIO_MODER_MODE3_Pos)          // DEBUG_RX       AF
                   | (2ul << GPIO_MODER_MODE11_Pos)         // CAN_RX         AF
                   | (2ul << GPIO_MODER_MODE12_Pos)         // CAN_TX         AF
                   | (2ul << GPIO_MODER_MODE13_Pos)         // SWDIO          AF
                   | (2ul << GPIO_MODER_MODE14_Pos)         // SWCLK          AF
                   | (2ul << GPIO_MODER_MODE15_Pos);        // JTDI           AF
    GPIOB->MODER = (2ul << GPIO_MODER_MODE3_Pos)            // SWO            AF
                   | (1ul << GPIO_MODER_MODE8_Pos);         // LED_YELLOW     Output

    crc_init();
    load_persistent_registers();
    console_init(0);
    init_fdcan();
}

void init_fdcan(void) {
    // Select PLL "Q" clock (160 MHz) as FDCAN clock source
    MODIFY_REG(RCC->CCIPR, RCC_CCIPR_FDCANSEL, (0b01ul << RCC_CCIPR_FDCANSEL_Pos));
    fdcan_clk = 160000000UL;
}

void reinit_fdcan(void) {
    PERIPH_BIT(RCC->APB1RSTR1, RCC_APB1RSTR1_FDCANRST_Pos) = 1;
    PERIPH_BIT(RCC->APB1RSTR1, RCC_APB1RSTR1_FDCANRST_Pos) = 0;

    init_fdcan();
}

uint32_t boot_id[4];

void generate_boot_id(void) {
    for (int i = 0; i < 4; ++i) {
        while (!READ_BIT(RNG->SR, RNG_SR_DRDY)) {
            // wait for random data to be ready
        }
        boot_id[i] = RNG->DR;
    }
}

UAVCANRegisterDeclaration _reg_boot_id = {  // com.starcopter.boot_id
    .name        = "com.starcopter.boot_id",
    .type        = eUAVCANRegisterTypeNatural32Array,
    .mutable     = 0,
    .persistent  = 0,
    .constant    = 1,
    .len         = 4,
    .min         = NULL,
    .max         = NULL,
    .dflt        = NULL,
    .value       = (void*) boot_id,
    .update_hook = NULL,
    .lock        = NULL};

void USART2_IRQHandler(void) {
    console_irq_handler();
}

void vApplicationStackOverflowHook(TaskHandle_t xTask, char* pcTaskName) {
    LOG_CRITICAL("Stack Overflow for task %s at 0x%08lx", pcTaskName, (uint32_t) xTask);
    console_flush();
    WTF();
}

void vApplicationMallocFailedHook(void) {
    LOG_WARNING("call to pvPortMalloc() failed");
    console_flush();
    HALT_IF_DEBUGGING();
}
