/**
 * @file pwm-input.c
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief PWM Input Demo
 * @version 0.1
 * @date 2022-09-13
 *
 * @copyright Copyright (c) 2022 starcopter GmbH
 *
 * This software is distributed under the terms of the MIT License.
 *
 */

#include <main.h>
#include "apps.h"

#include <sclib/timekeeper.h>
#include <sclib/registry.h>
#include <starcopter/temp/PWMInput_0_1.h>

#define LOG_LEVEL LOG_LEVEL_DEBUG
#include <sclib/log.h>

static void publish_pwm_input(uint32_t period_us, uint32_t duty_cycle_us);

static void PWMInputTask(void* param) {
    uint32_t period_us = 0, duty_cycle_us = 1000;

    LOG_DEBUG("PWM Input module initialized.");

    for (;;) {
        vTaskDelay(pdMS_TO_TICKS(20));  // publish at 50 Hz, roughly

        // TODO: measure actual period and duty cycle
        if (++duty_cycle_us > 2000) duty_cycle_us = 1000;

        publish_pwm_input(period_us, duty_cycle_us);
    }

    WTF();
}

void start_pwm_input_task(void) {
    BaseType_t status = xTaskCreate(PWMInputTask, "PWM Input", 512, NULL, TaskPriorityNominal, NULL);
    ASSERT(status == pdPASS);
}

// uavcan.pub.pwm_input: starcopter.temp.PWMInput.0.1
volatile uint16_t pwm_input_subject_id     = SUBJECT_ID_DEFAULT;
static const char pwm_input_subject_type[] = starcopter_temp_PWMInput_0_1_FULL_NAME_AND_VERSION_;

static void publish_pwm_input(uint32_t period_us, uint32_t duty_cycle_us) {
    static uint_fast8_t transfer_id       = 0;
    const uint16_t      subject_id        = pwm_input_subject_id;
    static TickType_t   last_transmission = 0;
    if (subject_id > CANARD_SUBJECT_ID_MAX) return;
    const TickType_t now = xTaskGetTickCount();
    if (now - last_transmission < pdMS_TO_TICKS(10)) return;  // limit to max. 100 Hz publication rate

    starcopter_temp_PWMInput_0_1 obj = {.timestamp = {get_synchronized_timestamp_us()}, .pwm = {.count = 1}};
    obj.pwm.elements[0] =
        (starcopter_temp_PWM16_0_1){.duty_cycle_us = duty_cycle_us < UINT16_MAX ? duty_cycle_us : UINT16_MAX,
                                    .period_us     = period_us < UINT16_MAX ? period_us : UINT16_MAX};
    size_t   size = starcopter_temp_PWMInput_0_1_SERIALIZATION_BUFFER_SIZE_BYTES_;
    uint8_t* buf  = pvPortMalloc(size);
    ASSERT(buf);
    const int_fast8_t result = starcopter_temp_PWMInput_0_1_serialize_(&obj, buf, &size);
    ASSERT(result == NUNAVUT_SUCCESS);
    const CanardTransferMetadata meta = {.priority       = CanardPrioritySlow,
                                         .transfer_kind  = CanardTransferKindMessage,
                                         .port_id        = subject_id,
                                         .remote_node_id = CANARD_NODE_ID_UNSET,
                                         .transfer_id    = transfer_id++};
    node_publish(10000ul, &meta, size, buf);
    last_transmission = now;
    vPortFree(buf);
}

// uavcan.pub.pwm_input.id
UAVCANRegisterDeclaration _reg_pub_pwm_input_id = {
    .name        = "uavcan.pub.pwm_input.id",
    .type        = eUAVCANRegisterTypeNatural16Array,
    .mutable     = 1,
    .persistent  = 1,
    .constant    = 0,
    .len         = 1,
    .min         = NULL,
    .max         = NULL,
    .dflt        = (void*) &SUBJECT_ID_DEFAULT,
    .value       = (void*) &pwm_input_subject_id,
    .update_hook = NULL,
    .lock        = NULL,
};
// uavcan.pub.pwm_input.type
UAVCANRegisterDeclaration _reg_pub_pwm_input_type = {
    .name        = "uavcan.pub.pwm_input.type",
    .type        = eUAVCANRegisterTypeString,
    .mutable     = 0,
    .persistent  = 1,
    .constant    = 1,  // persistent, but not to be commited to NVM
    .len         = sizeof(pwm_input_subject_type) - 1,
    .min         = NULL,
    .max         = NULL,
    .dflt        = NULL,
    .value       = (void*) pwm_input_subject_type,
    .update_hook = NULL,
    .lock        = NULL,
};
