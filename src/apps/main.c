/**
 * @file main.c
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief Main Task Implementation
 * @version 0.1
 * @date 2021-12-03
 *
 * @copyright Copyright (c) 2021 starcopter GmbH
 *
 */

#include <main.h>
#include "apps.h"

#include <sclib/assert.h>
#include <sclib/console.h>
#include <canard/canard.h>
#include <canard/fdcan.h>
#include <canard/node.h>
#include <sclib/registry.h>
#include <sclib/timekeeper.h>

#include <uavcan/node/Heartbeat_1_0.h>

#define LOG_LEVEL LOG_LEVEL_TRACE
#include <sclib/log.h>

#include <timers.h>

static void Main(void* param);
void        print_platform_info(void);

void start_main_task(void) {
    static TaskHandle_t handle = NULL;
    ASSERT(handle == NULL);

    BaseType_t status = xTaskCreate(Main, "Main", 512, NULL, TaskPrioritySlow, &handle);
    ASSERT(status == pdPASS);
}

static void Main(void* param __unused) {
    {
        const UAVCANRegister_t* const reg = get_register_by_name("uavcan.node.id");
        ASSERT(reg);
        uint16_t* nid = (uint16_t*) reg->value;
        if (*nid > CANARD_NODE_ID_MAX) {
            *nid = UID->WORD1 % 79 + 20;  // deterministic pseudo-random number in [20, 99]
        }
    }

    node_init();
    print_platform_info();

    start_node_monitor_task();
    start_pwm_input_task();

    while (1) {
        vTaskDelay(pdMS_TO_TICKS(100));
    }
}

void print_platform_info(void) {
    const char* const name = image_hdr.name;
    char              image_version_string[VERSION_STRING_MAXSZ];
    image_render_version_string(image_version_string, &image_hdr.version, image_hdr.clean_build);
    const HWVersion_t* const hw = &device_configuration_block.release;
    LOG_NOTICE("%s %s on HW %c.%u%s", name, image_version_string, hw->major, hw->minor, dcb_validate() ? "" : " (?)");

    LOG_INFO("Git Rev %08lx%s, Build ID %08lx, CRC %08lx/%08lx", __builtin_bswap32(*(uint32_t*) &image_hdr.git_sha[0]),
             image_hdr.clean_build ? "" : "*",
             __builtin_bswap32(*(uint32_t*) &g_note_build_id.data[g_note_build_id.namesz]), image_hdr.header_crc,
             __image_crc);
    LOG_INFO("Running on device %08lx", __builtin_bswap32(*(uint32_t*) &device_configuration_block.uid[12]));

    extern uint32_t boot_id[4];
    LOG_DEBUG("Boot ID %08lx%08lx%08lx%08lx", boot_id[3], boot_id[2], boot_id[1], boot_id[0]);
}
