/**
 * @file apps.h
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief Common Application Header
 * @version 0.1
 * @date 2021-12-03
 *
 * @copyright Copyright (c) 2021 starcopter GmbH
 *
 */

#pragma once

#include <main.h>

#include <FreeRTOS.h>
#include <task.h>

#include <canard/node.h>

typedef enum Mode {
    // Normal operating mode.
    MODE_OPERATIONAL = 0,
    // Initialization is in progress; this mode is entered immediately after startup.
    MODE_INITIALIZATION = 1,
    // Calibration, self-test, etc.
    MODE_MAINTENANCE
} Mode;

typedef enum Health {
    // The component is functioning properly (nominal).
    HEALTH_NOMINAL = 0,
    // A critical parameter went out of range or the component encountered a minor failure that does not prevent
    // the subsystem from performing any of its real-time functions.
    HEALTH_ADVISORY = 1,
    // The component encountered a major failure and is performing in a degraded mode or outside of its designed
    // limitations.
    HEALTH_CAUTION = 2,
    // The component suffered a fatal malfunction and is unable to perform its intended function.
    HEALTH_WARNING = 3,
} Health;

typedef enum Readiness {
    READINESS_SLEEP   = 0,  // reg.udral.service.common.Readiness.0.1 SLEEP
    READINESS_STANDBY = 2,  // reg.udral.service.common.Readiness.0.1 STANDBY
    READINESS_ENGAGED = 3,  // reg.udral.service.common.Readiness.0.1 ENGAGED
    READINESS_TIMEOUT = 4,  // implicit
} Readiness;

void start_main_task(void);
void start_node_monitor_task(void);
void start_pwm_input_task(void);
