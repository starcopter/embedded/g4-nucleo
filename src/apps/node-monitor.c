/**
 * @file apps/uavcan-sandbox.c
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief Sandbox Application for UAVCAN Integration Testing
 * @version 0.1
 *
 * @copyright Copyright (c) 2021 starcopter GmbH
 *
 * This software is distributed under the terms of the MIT License.
 *
 */

#include <apps/apps.h>

#include <timers.h>

#include <canard/canard.h>
#include <canard/fdcan.h>
#include <canard/node.h>
#include <sclib/registry.h>
#include <sclib/flash.h>
#include <sclib/memory_map.h>
#include <sclib/image.h>
#include <sclib/shared_ram.h>
#include <sclib/timekeeper.h>

#include <uavcan/node/Heartbeat_1_0.h>
#include <uavcan/node/Mode_1_0.h>
#include <uavcan/node/Health_1_0.h>

#define LOG_LEVEL LOG_LEVEL_INFO
#include <sclib/log.h>

static uint_fast8_t mode   = uavcan_node_Mode_1_0_INITIALIZATION;
static uint_fast8_t health = uavcan_node_Health_1_0_NOMINAL;

static TimerHandle_t led_fade_out;
static void          led_fade_out_callback(TimerHandle_t th);
void                 flash_led(const uint32_t duration_ms);

uint_fast8_t get_system_mode(void) {
    const uint_fast8_t node_mode      = node_get_mode();
    const uint_fast8_t transport_mode = transport_get_mode();

    if (mode == uavcan_node_Mode_1_0_INITIALIZATION || transport_mode == uavcan_node_Mode_1_0_INITIALIZATION ||
        node_mode == uavcan_node_Mode_1_0_INITIALIZATION)
    {
        return uavcan_node_Mode_1_0_INITIALIZATION;
    }

    const uint_fast8_t ext_mode = node_mode > transport_mode ? node_mode : transport_mode;
    return ext_mode > mode ? ext_mode : mode;
}

uint_fast8_t get_system_health(void) {
    const uint_fast8_t transport_health = transport_get_health();
    return health > transport_health ? health : transport_health;
}

static void Monitor(void* param);

void start_node_monitor_task(void) {
    static TaskHandle_t handle = NULL;
    ASSERT(handle == NULL);  // This function may only be called once
    BaseType_t rv = xTaskCreate(Monitor, "Node Monitor", 1024, NULL, TaskPriorityLow, &handle);
    ASSERT(rv == pdPASS);
}

typedef struct {
    uint8_t last   : 3;
    uint8_t online : 1;
    uint8_t uptime : 3;
    uint8_t seen   : 1;
} NodeStatusEntry;

static void Monitor(void* param __unused) {
    const TickType_t HEARTBEAT_PERIOD  = pdMS_TO_TICKS(uavcan_node_Heartbeat_1_0_MAX_PUBLICATION_PERIOD * 1000);
    const TickType_t HEARTBEAT_TIMEOUT = pdMS_TO_TICKS(uavcan_node_Heartbeat_1_0_OFFLINE_TIMEOUT * 1000);

    NodeStatusEntry nodes[CANARD_NODE_ID_MAX + 1] = {0};

    LOG_TRACE("subscribe to uavcan.node.Heartbeat.1.0");
    const uint32_t   HEARTBEAT_MAILBOX      = 1;
    NodeSubscription heartbeat_subscription = {.task = xTaskGetCurrentTaskHandle(), .mailbox = HEARTBEAT_MAILBOX};
    node_subscribe(uavcan_node_Heartbeat_1_0_FIXED_PORT_ID_, uavcan_node_Heartbeat_1_0_EXTENT_BYTES_,
                   &heartbeat_subscription);

    led_fade_out = xTimerCreate("LED Fade Out", portMAX_DELAY, pdFALSE, NULL, led_fade_out_callback);

    TickType_t last_heartbeat_received = 0;

    for (;;) {
        const CanardRxTransfer* transfer      = node_poll(&heartbeat_subscription, HEARTBEAT_PERIOD * 5 / 4);
        const uint8_t           truncated_now = (xTaskGetTickCount() / pdMS_TO_TICKS(1000)) & 0xf;

        if (transfer != NULL) {
            ASSERT(transfer->metadata.port_id == uavcan_node_Heartbeat_1_0_FIXED_PORT_ID_);

            mode = uavcan_node_Mode_1_0_OPERATIONAL;
            flash_led(50);
            last_heartbeat_received = xTaskGetTickCount();
            uavcan_node_Heartbeat_1_0 beat;
            size_t                    size = transfer->payload_size;
            const int_fast8_t ds_status    = uavcan_node_Heartbeat_1_0_deserialize_(&beat, transfer->payload, &size);
            ASSERT(ds_status == 0);
            LOG_DEBUG("Heartbeat %02u from Node %u received, uptime was %lu", transfer->metadata.transfer_id,
                      transfer->metadata.remote_node_id, beat.uptime);
            if (!nodes[transfer->metadata.remote_node_id].online) {
                LOG_NOTICE("Node %u %sappeared", transfer->metadata.remote_node_id,
                           nodes[transfer->metadata.remote_node_id].seen ? "re" : "");
                nodes[transfer->metadata.remote_node_id].online = 1;
            } else if (beat.uptime <= nodes[transfer->metadata.remote_node_id].uptime) {
                LOG_NOTICE("Node %u rebooted", transfer->metadata.remote_node_id);
            }
            nodes[transfer->metadata.remote_node_id].uptime = beat.uptime < 7 ? beat.uptime : 7;
            nodes[transfer->metadata.remote_node_id].last   = truncated_now;

            vPortFree((void*) transfer->payload);
            vPortFree((void*) transfer);

            // we've received a heartbeat, we should be good
            shared_reset_boot_count();
        }

        uint_fast8_t other_nodes_online = 0;
        for (uint_fast8_t i = 0; i <= CANARD_NODE_ID_MAX; i++) {
            if (!nodes[i].online) continue;
            if (((truncated_now - nodes[i].last) & (uint8_t) 7) > (HEARTBEAT_TIMEOUT / 1000)) {
                LOG_NOTICE("Node %u went dark", i);
                *(uint8_t*) &nodes[i] = 128;  // seen
                continue;
            }
            ++other_nodes_online;
        }

        if (other_nodes_online) {
            health = uavcan_node_Health_1_0_NOMINAL;
        } else {
            if (last_heartbeat_received + HEARTBEAT_TIMEOUT < xTaskGetTickCount()) {
                // timeout
                health = uavcan_node_Health_1_0_CAUTION;
            } else {
                health = uavcan_node_Health_1_0_ADVISORY;
            }
        }

        node_modify_vssc(0xffu, other_nodes_online);
    }
}

void flash_led(const uint32_t duration_ms) {
    xTimerChangePeriod(led_fade_out, pdMS_TO_TICKS(duration_ms), portMAX_DELAY);
    xTimerReset(led_fade_out, portMAX_DELAY);
    GPIOB->BSRR = GPIO_BSRR_BS8;
}
static void led_fade_out_callback(TimerHandle_t th) {
    GPIOB->BSRR = GPIO_BSRR_BR8;
}
