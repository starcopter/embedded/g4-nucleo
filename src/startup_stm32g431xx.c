/**
 * @file startup_stm32g431xx.c
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief CMSIS Startup File for STM32G431xx
 *
 * @copyright Copyright (c) 2021 starcopter GmbH
 *
 * This software is distributed under the terms of the MIT License.
 *
 * Modelled after the CMSIS Core Template [1] and ST's startup_stm32g431xx.s.
 *
 * [1]: https://www.keil.com/pack/doc/CMSIS/Core/html/startup_c_pg.html
 */

#include <stm32g4xx.h>

// defined in linker script
extern uint32_t __data_load__;
extern uint32_t __data_start__;
extern uint32_t __data_end__;
extern uint32_t __bss_start__;
extern uint32_t __bss_end__;
extern uint32_t __StackTop;
extern uint32_t __HeapBase;
extern uint32_t __HeapLimit;

typedef void (*pFunc)(void);
extern const pFunc __VECTOR_TABLE[118];

__NO_RETURN void Reset_Handler(void) {

    extern void __libc_init_array(void);
    extern int main(void);

    uint32_t* src_ptr = &__data_load__;
    uint32_t* dst_ptr = &__data_start__;

    if (src_ptr != dst_ptr) {
        while (dst_ptr < &__data_end__) {
            *dst_ptr++ = *src_ptr++;
        }
    }

    for (uint32_t* bss_ptr = &__bss_start__; bss_ptr < &__bss_end__;) {
        *bss_ptr++ = 0;
    }

    for (uint32_t* std_heap_ptr = &__HeapBase; std_heap_ptr < &__HeapLimit;) {
        *std_heap_ptr++ = (uint32_t) '*';
    }

    SystemInit();

    /* Cortex™-M4 Devices, Generic User Guide, Section 4.3.4 Vector Table Offset Register
     * "Table alignment requirements mean that bits[6:0] of the table offset are always zero" */
    SCB->VTOR = ((uint32_t) &__VECTOR_TABLE & 0xffffff80ul);

    __libc_init_array();

    main();

    __BKPT(1);
    while (1) {
        // infinite loop
    }
}

__NO_RETURN void Default_Handler(void) {
    __BKPT(0);
    while (1) {
        // hang indefinitely
    }
}

void NMI_Handler(void)                   __attribute__((weak, noreturn, alias("Default_Handler")));
void HardFault_Handler(void)             __attribute__((weak, noreturn, alias("Default_Handler")));
void MemManage_Handler(void)             __attribute__((weak, noreturn, alias("Default_Handler")));
void BusFault_Handler(void)              __attribute__((weak, noreturn, alias("Default_Handler")));
void UsageFault_Handler(void)            __attribute__((weak, noreturn, alias("Default_Handler")));
void SVC_Handler(void)                   __attribute__((weak, noreturn, alias("Default_Handler")));
void DebugMon_Handler(void)              __attribute__((weak, noreturn, alias("Default_Handler")));
void PendSV_Handler(void)                __attribute__((weak, noreturn, alias("Default_Handler")));
void SysTick_Handler(void)               __attribute__((weak, noreturn, alias("Default_Handler")));
void WWDG_IRQHandler(void)               __attribute__((weak, noreturn, alias("Default_Handler")));
void PVD_PVM_IRQHandler(void)            __attribute__((weak, noreturn, alias("Default_Handler")));
void RTC_TAMP_LSECSS_IRQHandler(void)    __attribute__((weak, noreturn, alias("Default_Handler")));
void RTC_WKUP_IRQHandler(void)           __attribute__((weak, noreturn, alias("Default_Handler")));
void FLASH_IRQHandler(void)              __attribute__((weak, noreturn, alias("Default_Handler")));
void RCC_IRQHandler(void)                __attribute__((weak, noreturn, alias("Default_Handler")));
void EXTI0_IRQHandler(void)              __attribute__((weak, noreturn, alias("Default_Handler")));
void EXTI1_IRQHandler(void)              __attribute__((weak, noreturn, alias("Default_Handler")));
void EXTI2_IRQHandler(void)              __attribute__((weak, noreturn, alias("Default_Handler")));
void EXTI3_IRQHandler(void)              __attribute__((weak, noreturn, alias("Default_Handler")));
void EXTI4_IRQHandler(void)              __attribute__((weak, noreturn, alias("Default_Handler")));
void DMA1_Channel1_IRQHandler(void)      __attribute__((weak, noreturn, alias("Default_Handler")));
void DMA1_Channel2_IRQHandler(void)      __attribute__((weak, noreturn, alias("Default_Handler")));
void DMA1_Channel3_IRQHandler(void)      __attribute__((weak, noreturn, alias("Default_Handler")));
void DMA1_Channel4_IRQHandler(void)      __attribute__((weak, noreturn, alias("Default_Handler")));
void DMA1_Channel5_IRQHandler(void)      __attribute__((weak, noreturn, alias("Default_Handler")));
void DMA1_Channel6_IRQHandler(void)      __attribute__((weak, noreturn, alias("Default_Handler")));
void ADC1_2_IRQHandler(void)             __attribute__((weak, noreturn, alias("Default_Handler")));
void USB_HP_IRQHandler(void)             __attribute__((weak, noreturn, alias("Default_Handler")));
void USB_LP_IRQHandler(void)             __attribute__((weak, noreturn, alias("Default_Handler")));
void FDCAN1_IT0_IRQHandler(void)         __attribute__((weak, noreturn, alias("Default_Handler")));
void FDCAN1_IT1_IRQHandler(void)         __attribute__((weak, noreturn, alias("Default_Handler")));
void EXTI9_5_IRQHandler(void)            __attribute__((weak, noreturn, alias("Default_Handler")));
void TIM1_BRK_TIM15_IRQHandler(void)     __attribute__((weak, noreturn, alias("Default_Handler")));
void TIM1_UP_TIM16_IRQHandler(void)      __attribute__((weak, noreturn, alias("Default_Handler")));
void TIM1_TRG_COM_TIM17_IRQHandler(void) __attribute__((weak, noreturn, alias("Default_Handler")));
void TIM1_CC_IRQHandler(void)            __attribute__((weak, noreturn, alias("Default_Handler")));
void TIM2_IRQHandler(void)               __attribute__((weak, noreturn, alias("Default_Handler")));
void TIM3_IRQHandler(void)               __attribute__((weak, noreturn, alias("Default_Handler")));
void TIM4_IRQHandler(void)               __attribute__((weak, noreturn, alias("Default_Handler")));
void I2C1_EV_IRQHandler(void)            __attribute__((weak, noreturn, alias("Default_Handler")));
void I2C1_ER_IRQHandler(void)            __attribute__((weak, noreturn, alias("Default_Handler")));
void I2C2_EV_IRQHandler(void)            __attribute__((weak, noreturn, alias("Default_Handler")));
void I2C2_ER_IRQHandler(void)            __attribute__((weak, noreturn, alias("Default_Handler")));
void SPI1_IRQHandler(void)               __attribute__((weak, noreturn, alias("Default_Handler")));
void SPI2_IRQHandler(void)               __attribute__((weak, noreturn, alias("Default_Handler")));
void USART1_IRQHandler(void)             __attribute__((weak, noreturn, alias("Default_Handler")));
void USART2_IRQHandler(void)             __attribute__((weak, noreturn, alias("Default_Handler")));
void USART3_IRQHandler(void)             __attribute__((weak, noreturn, alias("Default_Handler")));
void EXTI15_10_IRQHandler(void)          __attribute__((weak, noreturn, alias("Default_Handler")));
void RTC_Alarm_IRQHandler(void)          __attribute__((weak, noreturn, alias("Default_Handler")));
void USBWakeUp_IRQHandler(void)          __attribute__((weak, noreturn, alias("Default_Handler")));
void TIM8_BRK_IRQHandler(void)           __attribute__((weak, noreturn, alias("Default_Handler")));
void TIM8_UP_IRQHandler(void)            __attribute__((weak, noreturn, alias("Default_Handler")));
void TIM8_TRG_COM_IRQHandler(void)       __attribute__((weak, noreturn, alias("Default_Handler")));
void TIM8_CC_IRQHandler(void)            __attribute__((weak, noreturn, alias("Default_Handler")));
void LPTIM1_IRQHandler(void)             __attribute__((weak, noreturn, alias("Default_Handler")));
void SPI3_IRQHandler(void)               __attribute__((weak, noreturn, alias("Default_Handler")));
void UART4_IRQHandler(void)              __attribute__((weak, noreturn, alias("Default_Handler")));
void TIM6_DAC_IRQHandler(void)           __attribute__((weak, noreturn, alias("Default_Handler")));
void TIM7_IRQHandler(void)               __attribute__((weak, noreturn, alias("Default_Handler")));
void DMA2_Channel1_IRQHandler(void)      __attribute__((weak, noreturn, alias("Default_Handler")));
void DMA2_Channel2_IRQHandler(void)      __attribute__((weak, noreturn, alias("Default_Handler")));
void DMA2_Channel3_IRQHandler(void)      __attribute__((weak, noreturn, alias("Default_Handler")));
void DMA2_Channel4_IRQHandler(void)      __attribute__((weak, noreturn, alias("Default_Handler")));
void DMA2_Channel5_IRQHandler(void)      __attribute__((weak, noreturn, alias("Default_Handler")));
void UCPD1_IRQHandler(void)              __attribute__((weak, noreturn, alias("Default_Handler")));
void COMP1_2_3_IRQHandler(void)          __attribute__((weak, noreturn, alias("Default_Handler")));
void COMP4_IRQHandler(void)              __attribute__((weak, noreturn, alias("Default_Handler")));
void CRS_IRQHandler(void)                __attribute__((weak, noreturn, alias("Default_Handler")));
void SAI1_IRQHandler(void)               __attribute__((weak, noreturn, alias("Default_Handler")));
void FPU_IRQHandler(void)                __attribute__((weak, noreturn, alias("Default_Handler")));
void RNG_IRQHandler(void)                __attribute__((weak, noreturn, alias("Default_Handler")));
void LPUART1_IRQHandler(void)            __attribute__((weak, noreturn, alias("Default_Handler")));
void I2C3_EV_IRQHandler(void)            __attribute__((weak, noreturn, alias("Default_Handler")));
void I2C3_ER_IRQHandler(void)            __attribute__((weak, noreturn, alias("Default_Handler")));
void DMAMUX_OVR_IRQHandler(void)         __attribute__((weak, noreturn, alias("Default_Handler")));
void DMA2_Channel6_IRQHandler(void)      __attribute__((weak, noreturn, alias("Default_Handler")));
void CORDIC_IRQHandler(void)             __attribute__((weak, noreturn, alias("Default_Handler")));
void FMAC_IRQHandler(void)               __attribute__((weak, noreturn, alias("Default_Handler")));


const pFunc __VECTOR_TABLE[118] __VECTOR_TABLE_ATTRIBUTE = {
/******  Cortex-M4 Application Entry Point ****************************************************************************/
    (pFunc)(&__StackTop),           /*      Initial Stack Pointer                                                     */
    Reset_Handler,                  /*      Reset Handler                                                             */
/******  Cortex-M4 Processor Interrupts *******************************************************************************/
    NMI_Handler,                    /* -14  Cortex-M4 Non Maskable Interrupt                                          */
    HardFault_Handler,              /* -13  Cortex-M4 Hard Fault Interrupt                                            */
    MemManage_Handler,              /* -12  Cortex-M4 Memory Management Interrupt                                     */
    BusFault_Handler,               /* -11  Cortex-M4 Bus Fault Interrupt                                             */
    UsageFault_Handler,             /* -10  Cortex-M4 Usage Fault Interrupt                                           */
    0,                              /*      Reserved                                                                  */
    0,                              /*      Reserved                                                                  */
    0,                              /*      Reserved                                                                  */
    0,                              /*      Reserved                                                                  */
    SVC_Handler,                    /*  -5  Cortex-M4 SV Call Interrupt                                               */
    DebugMon_Handler,               /*  -4  Cortex-M4 Debug Monitor Interrupt                                         */
    0,                              /*      Reserved                                                                  */
    PendSV_Handler,                 /*  -2  Cortex-M4 Pend SV Interrupt                                               */
    SysTick_Handler,                /*  -1  Cortex-M4 System Tick Interrupt                                           */
/******  STM32 specific Interrupts ************************************************************************************/
    WWDG_IRQHandler,                /*   0  Window WatchDog Interrupt                                                 */
    PVD_PVM_IRQHandler,             /*   1  PVD/PVM1/PVM2/PVM3/PVM4 through EXTI Line detection Interrupts            */
    RTC_TAMP_LSECSS_IRQHandler,     /*   2  RTC Tamper and TimeStamp and RCC LSE CSS interrupts through the EXTI      */
    RTC_WKUP_IRQHandler,            /*   3  RTC Wakeup interrupt through the EXTI line                                */
    FLASH_IRQHandler,               /*   4  FLASH global Interrupt                                                    */
    RCC_IRQHandler,                 /*   5  RCC global Interrupt                                                      */
    EXTI0_IRQHandler,               /*   6  EXTI Line0 Interrupt                                                      */
    EXTI1_IRQHandler,               /*   7  EXTI Line1 Interrupt                                                      */
    EXTI2_IRQHandler,               /*   8  EXTI Line2 Interrupt                                                      */
    EXTI3_IRQHandler,               /*   9  EXTI Line3 Interrupt                                                      */
    EXTI4_IRQHandler,               /*  10  EXTI Line4 Interrupt                                                      */
    DMA1_Channel1_IRQHandler,       /*  11  DMA1 Channel 1 global Interrupt                                           */
    DMA1_Channel2_IRQHandler,       /*  12  DMA1 Channel 2 global Interrupt                                           */
    DMA1_Channel3_IRQHandler,       /*  13  DMA1 Channel 3 global Interrupt                                           */
    DMA1_Channel4_IRQHandler,       /*  14  DMA1 Channel 4 global Interrupt                                           */
    DMA1_Channel5_IRQHandler,       /*  15  DMA1 Channel 5 global Interrupt                                           */
    DMA1_Channel6_IRQHandler,       /*  16  DMA1 Channel 6 global Interrupt                                           */
    0,                              /*      Reserved                                                                  */
    ADC1_2_IRQHandler,              /*  18  ADC1 and ADC2 global Interrupt                                            */
    USB_HP_IRQHandler,              /*  19  USB HP Interrupt                                                          */
    USB_LP_IRQHandler,              /*  20  USB LP  Interrupt                                                         */
    FDCAN1_IT0_IRQHandler,          /*  21  FDCAN1 IT0 Interrupt                                                      */
    FDCAN1_IT1_IRQHandler,          /*  22  FDCAN1 IT1 Interrupt                                                      */
    EXTI9_5_IRQHandler,             /*  23  External Line[9:5] Interrupts                                             */
    TIM1_BRK_TIM15_IRQHandler,      /*  24  TIM1 Break, Transition error, Index error and TIM15 global interrupt      */
    TIM1_UP_TIM16_IRQHandler,       /*  25  TIM1 Update Interrupt and TIM16 global interrupt                          */
    TIM1_TRG_COM_TIM17_IRQHandler,  /*  26  TIM1 Trigger, Commutation, Dir. change, Index and TIM17 global interrupt  */
    TIM1_CC_IRQHandler,             /*  27  TIM1 Capture Compare Interrupt                                            */
    TIM2_IRQHandler,                /*  28  TIM2 global Interrupt                                                     */
    TIM3_IRQHandler,                /*  29  TIM3 global Interrupt                                                     */
    TIM4_IRQHandler,                /*  30  TIM4 global Interrupt                                                     */
    I2C1_EV_IRQHandler,             /*  31  I2C1 Event Interrupt                                                      */
    I2C1_ER_IRQHandler,             /*  32  I2C1 Error Interrupt                                                      */
    I2C2_EV_IRQHandler,             /*  33  I2C2 Event Interrupt                                                      */
    I2C2_ER_IRQHandler,             /*  34  I2C2 Error Interrupt                                                      */
    SPI1_IRQHandler,                /*  35  SPI1 global Interrupt                                                     */
    SPI2_IRQHandler,                /*  36  SPI2 global Interrupt                                                     */
    USART1_IRQHandler,              /*  37  USART1 global Interrupt                                                   */
    USART2_IRQHandler,              /*  38  USART2 global Interrupt                                                   */
    USART3_IRQHandler,              /*  39  USART3 global Interrupt                                                   */
    EXTI15_10_IRQHandler,           /*  40  External Line[15:10] Interrupts                                           */
    RTC_Alarm_IRQHandler,           /*  41  RTC Alarm (A and B) through EXTI Line Interrupt                           */
    USBWakeUp_IRQHandler,           /*  42  USB Wakeup through EXTI line Interrupt                                    */
    TIM8_BRK_IRQHandler,            /*  43  TIM8 Break, Transition error and Index error Interrupt                    */
    TIM8_UP_IRQHandler,             /*  44  TIM8 Update Interrupt                                                     */
    TIM8_TRG_COM_IRQHandler,        /*  45  TIM8 Trigger, Commutation, Direction change and Index Interrupt           */
    TIM8_CC_IRQHandler,             /*  46  TIM8 Capture Compare Interrupt                                            */
    0,                              /*      Reserved                                                                  */
    0,                              /*      Reserved                                                                  */
    LPTIM1_IRQHandler,              /*  49  LP TIM1 Interrupt                                                         */
    0,                              /*      Reserved                                                                  */
    SPI3_IRQHandler,                /*  51  SPI3 global Interrupt                                                     */
    UART4_IRQHandler,               /*  52  UART4 global Interrupt                                                    */
    0,                              /*      Reserved                                                                  */
    TIM6_DAC_IRQHandler,            /*  54  TIM6 global and DAC1&3 underrun error  interrupts                         */
    TIM7_IRQHandler,                /*  55  TIM7 global interrupts                                                    */
    DMA2_Channel1_IRQHandler,       /*  56  DMA2 Channel 1 global Interrupt                                           */
    DMA2_Channel2_IRQHandler,       /*  57  DMA2 Channel 2 global Interrupt                                           */
    DMA2_Channel3_IRQHandler,       /*  58  DMA2 Channel 3 global Interrupt                                           */
    DMA2_Channel4_IRQHandler,       /*  59  DMA2 Channel 4 global Interrupt                                           */
    DMA2_Channel5_IRQHandler,       /*  60  DMA2 Channel 5 global Interrupt                                           */
    0,                              /*      Reserved                                                                  */
    0,                              /*      Reserved                                                                  */
    UCPD1_IRQHandler,               /*  63  UCPD global Interrupt                                                     */
    COMP1_2_3_IRQHandler,           /*  64  COMP1, COMP2 and COMP3 Interrupts                                         */
    COMP4_IRQHandler,               /*  65  COMP4                                                                     */
    0,                              /*      Reserved                                                                  */
    0,                              /*      Reserved                                                                  */
    0,                              /*      Reserved                                                                  */
    0,                              /*      Reserved                                                                  */
    0,                              /*      Reserved                                                                  */
    0,                              /*      Reserved                                                                  */
    0,                              /*      Reserved                                                                  */
    0,                              /*      Reserved                                                                  */
    0,                              /*      Reserved                                                                  */
    CRS_IRQHandler,                 /*  75  CRS global interrupt                                                      */
    SAI1_IRQHandler,                /*  76  Serial Audio Interface global interrupt                                   */
    0,                              /*      Reserved                                                                  */
    0,                              /*      Reserved                                                                  */
    0,                              /*      Reserved                                                                  */
    0,                              /*      Reserved                                                                  */
    FPU_IRQHandler,                 /*  81  FPU global interrupt                                                      */
    0,                              /*      Reserved                                                                  */
    0,                              /*      Reserved                                                                  */
    0,                              /*      Reserved                                                                  */
    0,                              /*      Reserved                                                                  */
    0,                              /*      Reserved                                                                  */
    0,                              /*      Reserved                                                                  */
    0,                              /*      Reserved                                                                  */
    0,                              /*      Reserved                                                                  */
    RNG_IRQHandler,                 /*  90  RNG global interrupt                                                      */
    LPUART1_IRQHandler,             /*  91  LP UART 1 Interrupt                                                       */
    I2C3_EV_IRQHandler,             /*  92  I2C3 Event Interrupt                                                      */
    I2C3_ER_IRQHandler,             /*  93  I2C3 Error interrupt                                                      */
    DMAMUX_OVR_IRQHandler,          /*  94  DMAMUX overrun global interrupt                                           */
    0,                              /*      Reserved                                                                  */
    0,                              /*      Reserved                                                                  */
    DMA2_Channel6_IRQHandler,       /*  97  DMA2 Channel 6 interrupt                                                  */
    0,                              /*      Reserved                                                                  */
    0,                              /*      Reserved                                                                  */
    CORDIC_IRQHandler,              /* 100  CORDIC global Interrupt                                                   */
    FMAC_IRQHandler,                /* 101  FMAC global Interrupt                                                     */
};
